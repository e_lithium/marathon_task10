
let personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,

    start: function(){
        let numberOfFilms = NaN;
        while ((isNaN(numberOfFilms)) || (numberOfFilms === null) || (numberOfFilms == '')){
            numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');
        }
        personalMovieDB.count = numberOfFilms;
    },

    rememberMyFilms: function(){
        let movieCounter = 0;
        while (movieCounter < 2){
            const movie = prompt('Один из последних просмотренных фильмов?', '');
            const movieRating = +prompt('На сколько оцените его?', '');
            if ((movie !== null) && (movieRating !== null) && !(isNaN(movieRating)) && (movie != '') && (movieRating !== '') && (movie.length <= 50)){
                personalMovieDB.movies[movie] = +movieRating;
                movieCounter++;
            }
        }
    },

    detectPersonalLevel: function(){
        if (personalMovieDB.count < 10){
            alert("Просмотрено довольно мало фильмов")
        } else if (personalMovieDB.count <= 30) {
            alert("Вы классический зритель");
        } else if (personalMovieDB.count > 30) {
            alert("Вы киноман");
        } else {
            alert("Произошла ошибка");
        }
    },

    showMyDB: function(){
        if (personalMovieDB.privat == false){
            console.log(personalMovieDB);
        }
    },

    writeYourGenres: function(){
        let genreNum = 1;
        while (genreNum != 4){
            const genre = prompt(`Ваш любимый жанр под номером ${genreNum}?`, '');
            if ((genre !== null) && (genre != '')){
                personalMovieDB.genres[genreNum - 1] = genre;
                genreNum++;
            }
        }
        personalMovieDB.genres.forEach(function(item, i) {
            console.log(`Любимый жанр ${i + 1} - это ${item}`);
        });
    },

    toggleVisibleMyDB: function(){
        if (personalMovieDB.privat == true){
            personalMovieDB.privat = false;
        } else {
            personalMovieDB.privat = true;
        }
    }
};

personalMovieDB.start();
personalMovieDB.detectPersonalLevel();

personalMovieDB.showMyDB();
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();

personalMovieDB.writeYourGenres();